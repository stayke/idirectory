<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function betaDocs($docs = ""){
    	$resources = \DB::table('resources')->get();
    	$resources = json_encode($resources);
    	//var_dump($resources);
    	//echo "Beta Api invoked, docs: $docs";
    	return view('docs.beta.docs', compact('resources'));
    }
    public function v10Docs($docs = ""){
    	echo "Version 1.0 Api invoked, docs: $docs";
    }
}
